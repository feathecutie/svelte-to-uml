#!/usr/bin/env bash
find docs -type f -name *.plantuml -exec plantuml -tsvg {} \; 
mkdocs build

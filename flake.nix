{
  inputs.catppuccin-vsc.url = "github:catppuccin/vscode";

  outputs = { nixpkgs, flake-utils, catppuccin-vsc, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default = pkgs.mkShell {
          packages = [
            pkgs.nodejs_21
            pkgs.nodePackages.pnpm
            pkgs.plantuml
            pkgs.pandoc
            pkgs.librsvg
            pkgs.texlive.combined.scheme-medium
            pkgs.python311
            pkgs.python311Packages.weasyprint
            pkgs.python311Packages.mkdocs-material
            pkgs.mkdocs
            pkgs.python311Packages.beautifulsoup4
            pkgs.python311Packages.libsass
            (
              pkgs.python311Packages.buildPythonPackage rec {
                pname = "mkdocs-with-pdf";
                version = "0.9.3";
                src = pkgs.fetchPypi {
                  inherit pname version;
                  sha256 = "sha256-vaM3XXBA0biHHaF8bXHqc2vcpsZpYI8o7WJ3EDHS4MY=";
                };
                doCheck = false;
                propagatedBuildInputs = [
                  # Specify dependencies
                  # pkgs.python3Packages.numpy
                ];
              }
            )
          ];
        };
        packages.default = catppuccin-vsc.packages.${system}.default.override {
          bracketMode = "rainbow";
        };
      });
}

# svelte-to-uml

Converts a SvelteKit project to a PlantUML class diagram! Yay!

## Example Usage

```bash
echo @feathecutie:registry=https://gitlab.com/api/v4/projects/52994631/packages/npm/ >> .npmrc
npx @feathecutie/svelte-to-uml@latest ./path-to-project ./path-to-output-file.plantuml
```

Note that `npm install` or an equivalent command has to be ran in the SvelteKit project at least once before using this.


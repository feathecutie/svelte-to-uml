import { Context as C, Effect as E } from "effect";
import {
  Options,
  Program,
  ProjectPath,
  SvelteFileData,
  OutPath,
} from "./types.js";
import {
  isSvelteFilepath,
  isVirtualSvelteFilepath,
  writeToFileSync,
} from "./utils.js";
import { resolveModuleName } from "./init-ts.js";
import ts from "typescript";
import path from "node:path";
import { writePlantUML } from "./uml.js";
import { generateDocs } from "./docs.js";
import processSvelteFile from "./processSvelteFile.js";

export class SourceFile extends C.Tag("SourceFile")<
  SourceFile,
  ts.SourceFile
>() {}

export default E.gen(function* (_) {
  console.log("....started the program");

  const program = yield* _(Program);

  const filter = (sf: ts.SourceFile) =>
    E.gen(function* (_) {
      return (
        (isSvelteFilepath(sf.fileName) ||
          isVirtualSvelteFilepath(sf.fileName) ||
          sf.fileName.endsWith(".svelte.d.ts")) &&
        !sf.fileName.includes(".story.svelte") &&
        !resolveModuleName(
          sf.fileName,
          yield* _(ProjectPath),
          yield* _(Options)
        )?.packageId
      );
    });

  const svelteFiles = yield* _(program.getSourceFiles(), E.filter(filter));

  const processed: SvelteFileData[] = yield* _(
    svelteFiles,
    E.forEach(processSvelteFile)
  );

  console.log("finished processing svelte files");

  const filtered = processed
    .filter(
      (d) => d.type !== "component" || d.id.startsWith("src/lib/components")
    )
    .map(
      (d) =>
        ({
          ...d,
          importIds: d.importIds.filter(
            (id) =>
              processed.find((p) => p.id === id)!.type !== "component" ||
              id.startsWith("src/lib/components")
          ),
        } as SvelteFileData)
    );

  console.log("finished filtering processed svelte files");

  yield* _(generateDocs(filtered));

  console.log("finished generating docs");

  const uml = yield* _(E.promise(() => writePlantUML(filtered)));

  console.log("finished uml stuff");

  const outPath = yield* _(OutPath);
  writeToFileSync(path.join(outPath, "uml.plantuml"), uml);
});

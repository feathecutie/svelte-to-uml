import { Effect as E, Brand as B, Option as O, Match as M } from "effect";
import {
  Options,
  Program,
  ProjectPath,
  SvelteFileData,
  TypeChecker,
} from "./types.js";
import path from "node:path";
import ts from "typescript";
import { resolveModuleName } from "./init-ts.js";

export type Id = B.Branded<string, "Id">;
export const IdEffect = ({ sf }: { sf: ts.SourceFile }) =>
  E.gen(function* (_) {
    return B.nominal<Id>()(
      path.relative(
        yield* _(ProjectPath),
        sf.fileName
          .replace(".svelte.js", ".svelte")
          .replace(".svelte.ts", ".svelte")
          .replace(".svelte.d.ts", ".svelte")
      )
    );
  });

export const PackageIdEffect = ({ sf }: { sf: ts.SourceFile }) =>
  E.gen(function* (_) {
    return O.fromNullable(
      resolveModuleName(sf.fileName, yield* _(ProjectPath), yield* _(Options))
        ?.packageId
    );
  });

export type DefaultExportValue = B.Branded<
  ts.Declaration,
  "DefaultExportValue"
>;
export const DefaultExportValueEffect = ({ sf }: { sf: ts.SourceFile }) =>
  E.gen(function* (_) {
    const tc = yield* _(TypeChecker);
    const defaultExportValue = tc
      .getSymbolAtLocation(sf)!
      .exports!.get("default" as ts.__String)!.valueDeclaration!;
    return B.nominal<DefaultExportValue>()(defaultExportValue);
  });

export type DefaultExportType = B.Branded<ts.Type, "DefaultExportType">;
export const DefaultExportTypeEffect = ({
  defaultExportValue,
}: {
  defaultExportValue: DefaultExportValue;
}) =>
  E.gen(function* (_) {
    const tc = yield* _(TypeChecker);
    const defaultExportType = tc.getTypeAtLocation(defaultExportValue);
    return B.nominal<DefaultExportType>()(defaultExportType);
  });

export type Generics = B.Branded<string[], "Generics">;
export const GenericsEffect = ({
  defaultExportValue,
}: {
  defaultExportValue: DefaultExportValue;
}) =>
  E.succeed(
    B.nominal<Generics>()(
      ts.isClassDeclaration(defaultExportValue)
        ? (defaultExportValue.typeParameters ?? []).map((tp) => {
            const constraint = ts.getEffectiveConstraintOfTypeParameter(tp);
            const name = tp.name.getText();
            return constraint
              ? `${name} extends ${constraint.getText()}`
              : name;
          })
        : []
    )
  );

export type Slots = B.Branded<string[], "Slots">;
export const SlotsEffect = ({
  defaultExportType,
}: {
  defaultExportType: DefaultExportType;
}) =>
  E.gen(function* (_) {
    const tc = yield* _(TypeChecker);
    return B.nominal<Slots>()(
      tc
        .getTypeOfSymbol(defaultExportType.getProperty("$$slot_def")!)
        .getProperties()
        .map((slot) => slot.name)
    );
  });

export type Documentation = B.Branded<
  readonly (ts.JSDoc | ts.JSDocTag)[],
  "Documentation"
>;
export const DocumentationEffect = ({
  defaultExportValue,
}: {
  defaultExportValue: DefaultExportValue;
}) =>
  E.succeed(
    B.nominal<Documentation>()(ts.getJSDocCommentsAndTags(defaultExportValue))
  );

export type Props = B.Branded<
  {
    name: string;
    type: string;
    documentation: Documentation;
  }[],
  "Props"
>;
export const PropsEffect = ({
  sf,
  defaultExportType,
}: {
  sf: ts.SourceFile;
  defaultExportType: DefaultExportType;
}) =>
  E.gen(function* (_) {
    const tc = yield* _(TypeChecker);
    return B.nominal<Props>()(
      tc
        .getTypeOfSymbol(defaultExportType.getProperty("$$prop_def")!)
        .getProperties()
        .map((prop) => {
          const type = tc.getTypeOfSymbol(prop);
          return {
            name: prop.name,
            type: tc.typeToString(type, sf),
            documentation: B.nominal<Documentation>()(
              ts.getJSDocCommentsAndTags(prop.valueDeclaration!)
            ),
          };
        })
    );
  });

export type Events = B.Branded<{ name: string; type: string }[], "Events">;
export const EventsEffect = ({
  defaultExportType,
}: {
  defaultExportType: DefaultExportType;
}) =>
  E.gen(function* (_) {
    const tc = yield* _(TypeChecker);
    return B.nominal<Events>()(
      tc
        .getTypeOfSymbol(defaultExportType.getProperty("$$events_def")!)
        .getProperties()
        .map((event) => ({
          name: event.name,
          type: tc.typeToString(tc.getTypeOfSymbol(event)),
        }))
    );
  });

export type Type = "page" | "layout" | "error" | "component";
export const TypeEffect = ({ id }: { id: Id }) =>
  E.succeed(
    M.value(path.parse(id).name).pipe(
      M.when("+page", () => "page" as const),
      M.when("+layout", () => "layout" as const),
      M.when("+error", () => "error" as const),
      M.orElse(() => "component" as const)
    )
  );

export type Name = B.Branded<string, "Name">;
export const NameEffect = ({ type, id }: { type: Type; id: Id }) =>
  E.sync(() => {
    const parsed = path.parse(id);
    const routeName =
      "/" +
      path
        .relative("src/routes", parsed.dir)
        .split("/")
        .filter((s) => !/\(.+?\)/.test(s))
        .join("/");
    return B.nominal<Name>()(type !== "component" ? routeName : parsed.name);
  });

export type ImportIds = B.Branded<Id[], "ImportIds">;
export const ImportIdsEffect = ({ sf }: { sf: ts.SourceFile }) =>
  E.gen(function* (_) {
    const getImports = (
      node: ts.Node
    ): E.Effect<string[], never, TypeChecker> =>
      E.gen(function* (_) {
        const strings: string[] = [];
        const tc = yield* _(TypeChecker);
        if (ts.isImportDeclaration(node) && !node.importClause?.isTypeOnly) {
          const text = (node.moduleSpecifier as ts.StringLiteral).text;
          if (text.endsWith(".svelte")) {
            strings.push(text);
          } else {
            const name = node.importClause?.name;
            if (name) {
              const sf = tc
                .getTypeAtLocation(name)
                .getSymbol()
                ?.valueDeclaration?.getSourceFile();

              if (
                sf &&
                (sf.fileName.endsWith(".svelte") ||
                  sf.fileName.endsWith(".svelte.d.ts"))
              ) {
                strings.push(sf.fileName);
              }
            }
            const bindings = node.importClause?.namedBindings;
            if (bindings && ts.isNamedImports(bindings)) {
              /*.filter(
                  (sf) => sf.endsWith(".svelte") || sf.endsWith(".svelte.d.ts")
                )*/ for (const sf of bindings.elements
                .map((idk) =>
                  tc
                    .getTypeAtLocation(idk)
                    .getSymbol()
                    ?.valueDeclaration?.getSourceFile()
                )
                .filter(Boolean)) {
                if (
                  sf.fileName.endsWith(".svelte") ||
                  sf.fileName.endsWith(".svelte.d.ts")
                ) {
                  strings.push(sf.fileName);
                }
              }
            }
          }
          return strings;
        }

        strings.push(
          ...(yield* _(
            node.getChildren(),
            E.forEach((c) => getImports(c)),
            E.map((a) => a.flat())
          ))
        );

        return strings;
      });
    const imports = yield* _(getImports(sf));
    const options = yield* _(Options);
    const program = yield* _(Program);
    const importModules = imports
      .map((i) => resolveModuleName(i, sf.fileName, options)!)
      .filter((m) => !m.packageId);
    return B.nominal<ImportIds>()(
      yield* _(
        importModules,
        E.forEach((i) =>
          IdEffect({ sf: program.getSourceFile(i.resolvedFileName)! })
        )
      )
    );
  });

export default (sf: ts.SourceFile) =>
  E.gen(function* (_) {
    const id = yield* _(IdEffect({ sf }));
    const packageId = yield* _(PackageIdEffect({ sf }));
    const defaultExportValue = yield* _(DefaultExportValueEffect({ sf }));
    const defaultExportType = yield* _(
      DefaultExportTypeEffect({ defaultExportValue })
    );
    const generics = yield* _(GenericsEffect({ defaultExportValue }));
    const slots = yield* _(SlotsEffect({ defaultExportType }));
    const props = yield* _(PropsEffect({ defaultExportType, sf }));
    const events = yield* _(EventsEffect({ defaultExportType }));
    const documentation = yield* _(DocumentationEffect({ defaultExportValue }));
    const importIds = yield* _(ImportIdsEffect({ sf }));
    const type = yield* _(TypeEffect({ id }));
    const name = yield* _(NameEffect({ type, id }));
    return {
      id,
      packageId,
      generics,
      slots,
      props,
      documentation,
      importIds,
      events,
      name,
      type,
    } as SvelteFileData;
  });

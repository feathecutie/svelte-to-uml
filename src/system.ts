import ts from "typescript";
import { preprocess } from "svelte/compiler";
import { svelte2tsx } from "svelte2tsx";
import {
  ensureRealSvelteFilepath,
  isSvelteFilepath,
  isVirtualSvelteFilepath,
  memoize,
} from "./utils.js";

const read = memoize((filePath) => {
  return ts.sys.readFile(filePath, "utf-8");
});

export const getIsTsFile = memoize((filePath) => {
  const content = read(filePath);
  if (content == undefined) throw new Error(`"${filePath}" couldn't be read`);
  let isTsFile = false;
  void preprocess(content, {
    name: "get-attributes",
    script: ({ attributes }) => {
      isTsFile = attributes.lang == "ts";
    },
  });
  return isTsFile;
});

const convert = memoize((filePath) => {
  const content = read(filePath);
  if (content == undefined) throw new Error(`"${filePath}" couldn't be read`);
  return svelte2tsx(content, {
    filename: filePath,
    isTsFile: getIsTsFile(filePath),
  }).code;
});

export default {
  ...ts.sys,
  fileExists: (filePath) => {
    const realPath = ensureRealSvelteFilepath(filePath);
    const exists = ts.sys.fileExists(realPath);
    if (exists && isSvelteFilepath(realPath)) {
      const isTsFile = getIsTsFile(realPath);
      if (
        (isTsFile && !filePath.endsWith(".ts")) ||
        (!isTsFile && !filePath.endsWith(".js"))
      )
        return false;
    }
    return exists;
  },
  readFile: (filePath, encoding) => {
    if (isVirtualSvelteFilepath(filePath) || isSvelteFilepath(filePath)) {
      const realPath = ensureRealSvelteFilepath(filePath);
      return convert(realPath);
    }
    return ts.sys.readFile(filePath, encoding);
  },
  readDirectory: (path, extensions, exclude, include, depth) => {
    return ts.sys.readDirectory(
      path,
      (extensions ?? []).concat(".svelte"),
      exclude,
      include,
      depth
    );
  },
  writeFile: () => {
    throw new Error("Typescript should never attempt to write files");
  },
} as ts.System;

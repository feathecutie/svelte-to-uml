import {
  Effect as E,
  Match as M,
  String as S,
  Array as A,
  Unify as U,
  pipe,
} from "effect";
import { OutPath, SvelteFileData, TypeChecker } from "./types.js";
import { writeToFileSync } from "./utils.js";
import { toMarkdown } from "mdast-util-to-markdown";
import path from "node:path";
import { RootContent } from "mdast";
import {
  headingNode,
  htmlNode,
  imageNode,
  inlineCodeNode,
  linkNode,
  paragraphNode,
  rootNode,
  textNode,
} from "./ast.js";
import ts from "typescript";
import { fromMarkdown } from "mdast-util-from-markdown";
import { generatePlantUMLClass, umlBoilerplate } from "./uml.js";
import {
  IdEffect,
  Name,
  NameEffect,
  Type,
  TypeEffect,
} from "./processSvelteFile.js";

const nameAndFileToTarget = (name: Name, type: Type) =>
  M.value(type).pipe(
    M.when("component", () => "components.md"),
    M.when("page", () => "pages.md"),
    M.when("layout", () => "layouts.md"),
    M.when("error", () => "errorpages.md"),
    M.exhaustive
  ) +
  "#" +
  name.toLowerCase().trim();

const jsDocCommentAst = (comment: string | ts.NodeArray<ts.JSDocComment>) =>
  typeof comment === "string"
    ? E.succeed(fromMarkdown(comment).children)
    : E.forEach(
        comment,
        U.unify((c) =>
          c.kind === ts.SyntaxKind.JSDocLink ||
          c.kind === ts.SyntaxKind.JSDocLinkCode ||
          c.kind === ts.SyntaxKind.JSDocLinkPlain
            ? E.gen(function* (_) {
                const tc = yield* _(TypeChecker);
                const sourceFile = tc
                  .getTypeAtLocation(c.name!)
                  .getSymbol()
                  ?.valueDeclaration?.getSourceFile();
                if (!sourceFile) {
                  yield* _(
                    E.logError(`Failed to resolve link: ${c.name?.getText()}`)
                  );
                  return linkNode({
                    url: "ERROR",
                    children: [textNode({ value: c.name!.getText() })],
                  });
                }
                const id = yield* _(IdEffect({ sf: sourceFile }));
                const type = yield* _(TypeEffect({ id }));
                const target = nameAndFileToTarget(
                  yield* _(
                    NameEffect({
                      type,
                      id,
                    })
                  ),
                  type
                );
                return linkNode({
                  url: target,
                  children: [textNode({ value: c.name!.getText() })],
                });
              })
            : E.succeed(htmlNode({ value: c.text }))
        )
      ).pipe(E.map((nodes) => [paragraphNode({ children: nodes })]));

const jsDocTagAst = (tag: ts.JSDocTag) =>
  E.gen(function* (_) {
    return [
      paragraphNode({
        children: [textNode({ value: `${S.capitalize(tag.tagName.text)}:` })],
      }),
      ...(tag.comment ? yield* _(jsDocCommentAst(tag.comment)) : []),
    ];
  });

const jsDocAst = (doc: readonly (ts.JSDoc | ts.JSDocTag)[]) =>
  E.forEach(doc, (n) =>
    "tagName" in n
      ? jsDocTagAst(n)
      : E.gen(function* (_) {
          return [
            ...(n.comment ? yield* _(jsDocCommentAst(n.comment)) : []),
            ...(yield* _(E.forEach(n.tags ?? [], jsDocTagAst))).flat(),
          ];
        })
  ).pipe(E.map((a) => a.flat()));

const ast = (data: readonly SvelteFileData[], targetType: Type) =>
  E.forEach(
    data.filter((d) => d.type === targetType),
    ({
      documentation,
      events,
      generics,
      id,
      importIds,
      name,
      packageId,
      props,
      slots,
      type,
    }) =>
      E.gen(function* (_) {
        const uml = yield* _(
          E.promise(() =>
            generatePlantUMLClass(
              {
                documentation,
                events,
                generics,
                id,
                importIds,
                name,
                packageId,
                props,
                slots,
                type,
              },
              false
            )
          )
        );
        const outPath = yield* _(OutPath);
        writeToFileSync(
          path.join(outPath, id) + ".plantuml",
          umlBoilerplate(uml)
        );

        return [
          headingNode({ depth: 2, children: [textNode({ value: name })] }),
          paragraphNode({
            children: [
              imageNode({
                url: id + ".svg",
                alt: `UML Representation of ${name}`,
              }),
            ],
          }),
          ...(importIds.length
            ? [
                paragraphNode({
                  children: [
                    textNode({
                      value: "Uses ",
                    }),
                    ...pipe(
                      importIds.map((importId) => {
                        const d = data.find((f) => f.id == importId);
                        if (!d)
                          throw new Error(`nothing found with id ${importId}`);
                        return linkNode({
                          url: nameAndFileToTarget(d.name, d.type),
                          children: [textNode({ value: d.name })],
                        });
                      }),
                      A.intersperse(textNode({ value: ", " }))
                    ),
                    textNode({ value: "." }),
                  ],
                }),
              ]
            : []),
          ...(yield* _(jsDocAst(documentation))),
          ...(props.length
            ? [
                headingNode({
                  depth: 3,
                  children: [textNode({ value: "Props" })],
                }),
                ...(generics.length
                  ? [
                      paragraphNode({
                        children: [
                          textNode({ value: "Generics: " }),
                          inlineCodeNode({
                            value: generics.join(),
                          }),
                        ],
                      }),
                    ]
                  : []),
                ...(yield* _(
                  E.forEach(props, ({ documentation, name, type: propType }) =>
                    E.gen(function* (_) {
                      return [
                        headingNode({
                          depth: 4,
                          children: [
                            textNode({ value: `${name}: ` }),
                            inlineCodeNode({ value: propType }),
                          ],
                        }),
                        ...fromMarkdown(
                          toMarkdown(
                            rootNode({
                              children: yield* _(jsDocAst(documentation)),
                            })
                          )
                        ).children,
                      ];
                    })
                  )
                )).flat(),
              ]
            : []),
          ...(slots.length
            ? [
                headingNode({
                  depth: 3,
                  children: [textNode({ value: "Slots" })],
                }),
                ...slots.flatMap((name) => [
                  headingNode({
                    depth: 4,
                    children: [textNode({ value: name })],
                  }),
                ]),
              ]
            : []),
          ...(events.length
            ? [
                headingNode({
                  depth: 3,
                  children: [textNode({ value: "Events" })],
                }),
                ...events.flatMap(({ name, type }) => [
                  headingNode({
                    depth: 4,
                    children: [
                      textNode({ value: `${name}: ` }),
                      inlineCodeNode({ value: type }),
                    ],
                  }),
                ]),
              ]
            : []),
        ];
      })
  ).pipe(E.map((a) => a.flat()));

export const generateDocs = (data: readonly SvelteFileData[]) =>
  E.gen(function* (_) {
    const outPath = yield* _(OutPath);

    const rootWithHeading = (value: string, children: readonly RootContent[]) =>
      rootNode({
        children: [
          headingNode({ depth: 1, children: [textNode({ value })] }),
          ...children,
        ],
      });

    writeToFileSync(
      path.join(outPath, "components.md"),
      toMarkdown(
        rootWithHeading("Components", yield* _(ast(data, "component")))
      )
    );

    writeToFileSync(
      path.join(outPath, "pages.md"),
      toMarkdown(rootWithHeading("Pages", yield* _(ast(data, "page"))))
    );

    writeToFileSync(
      path.join(outPath, "layouts.md"),
      toMarkdown(rootWithHeading("Layouts", yield* _(ast(data, "layout"))))
    );

    writeToFileSync(
      path.join(outPath, "errorpages.md"),
      toMarkdown(rootWithHeading("Error Pages", yield* _(ast(data, "error"))))
    );
  });

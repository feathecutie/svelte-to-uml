import { FontStyle, ThemeRegistration, codeToTokens } from "shiki";
import fs from "node:fs";
import path from "node:path";
import { match } from "ts-pattern";
import { SvelteFileData } from "./types.js";
import multiline from "multiline-ts";
import { opt } from "./utils.js";
import { Option as O, String as S } from "effect";

async function highlight(code: string) {
  const { tokens } = await codeToTokens("let a: " + code, {
    lang: "typescript",
    theme: JSON.parse(
      fs.readFileSync(new URL("../latte.json", import.meta.url), "utf-8")
    ) as ThemeRegistration,
  });
  return tokens
    .flat()
    .slice(3)
    .map((token) => {
      const textStyle = (c: string) =>
        match(token.fontStyle)
          .with(FontStyle.Bold, () => `<b>${c}</b>`)
          .with(FontStyle.Italic, () => `<i>${c}</i>`)
          .with(FontStyle.Underline, () => `<u>${c}</u>`)
          .otherwise(() => c);
      const textColor = (c: string) =>
        token.color ? `<color:${token.color}>${c}</color>` : c;
      return textColor(textStyle(token.content));
    })
    .join("");
}

const prototypeData: {
  [prototype in SvelteFileData["type"]]: {
    color: string;
    name: string;
    bgColor: string;
  };
} = {
  component: {
    color: "#9977DD",
    name: "Component",
    bgColor: "#f6f2ff",
  },
  error: {
    color: "#EE5555",
    name: "ErrorPage",
    bgColor: "#edd1e1",
  },
  layout: {
    color: "#BBEE00",
    name: "Layout",
    bgColor: "#f8ffe5",
  },
  page: { color: "#FF7700", name: "Page", bgColor: "#fff0e5" },
};

export async function generatePlantUMLClass(
  { events, generics, id, name, packageId, props, slots, type }: SvelteFileData,
  generateModuleString = true
) {
  const nullablePackageId = packageId.pipe(O.getOrNull);
  const moduleString = generateModuleString
    ? nullablePackageId
      ? `External::${nullablePackageId.name}`
      : type !== "component"
      ? // ? `Routes::${id
        //     .split("/")
        //     .slice(0, -1)
        //     .slice(2, 4)
        //     // .map((s) => "/" + s)
        //     .join("::")}`
        "Routes"
      : type === "component"
      ? `Components${opt`::${
          id.startsWith("src/lib/components")
            ? id
                .replace("src/lib/components/", "")
                .split("/")
                .slice(0, -1)
                .map(S.capitalize)
                .join("::")
            : undefined
        }`}`
      : undefined
    : undefined;
  const prototype = `(${prototypeData[type].name.charAt(0).toUpperCase()}, ${
    prototypeData[type].color
  }) ${prototypeData[type].name}`;
  const genericsString = generics.join(", ");
  const propsString = (
    await Promise.all(
      props.map(async ({ name, type: propType }) => {
        if (name === "data" && type === "page")
          return `+data: ${await highlight("PageData")}`;
        const stringToRemove = " | undefined";
        const optional = propType.endsWith(stringToRemove);
        const typeString = await highlight(
          optional ? propType.slice(0, -stringToRemove.length) : propType
        );
        return `+${name}${optional ? "?" : ""}: ${typeString}`;
      })
    )
  ).join("\n");
  const slotsString = slots.map((slot) => "+" + slot).join("\n");
  const eventsString = (
    await Promise.all(
      events.map(async ({ name, type }) => {
        return `+${name}: ${await highlight(type)}`;
      })
    )
  ).join("\n");
  return multiline`
    class "${name}" as ${opt`${moduleString}::`}${id}${opt`<${genericsString}>`} << ${prototype} >> {
      ${opt`__ Properties __\n${propsString}\n`}
      ${opt`__ Slots __\n${slotsString}\n`}
      ${opt`__ Events __\n${eventsString}`}
    }
  `;
}

export const umlBoilerplate = (uml: string) =>
  multiline`
    @startuml
    set separator ::
    skinparam groupInheritance 5
    skinparam packageStyle Rectangle
    skinparam class {
      ${Object.values(prototypeData)
        .map(({ name, bgColor }) => `BackgroundColor<<${name}>> ${bgColor}`)
        .join("\n")}
    }
    ${uml}
    @enduml
  `
    .split("\n")
    .filter((line) => !/^\s*$/.test(line))
    .join("\n");

export async function writePlantUML(data: SvelteFileData[]) {
  const { classes, relations } = (
    await Promise.all(
      data.map(
        async ({
          id,
          name,
          type,
          packageId,
          importIds,
          generics,
          props,
          documentation,
          slots,
          events,
        }) => {
          const classString = await generatePlantUMLClass({
            documentation,
            events,
            generics,
            id,
            importIds,
            name,
            packageId,
            props,
            slots,
            type,
          });

          const relations = (() => {
            const uses = importIds.map((im) => `"${id}" ..> "${im}"`);
            const inherits =
              type == "layout"
                ? data
                    .filter(
                      ({ type: pageType, id: pageId }) =>
                        pageType == "page" &&
                        !path
                          .relative(path.dirname(id), path.dirname(pageId))
                          .startsWith("..")
                    )
                    .map(({ id: pageId }) => `"${id}" <|-- "${pageId}"`)
                : [];
            return [...uses, ...inherits];
          })();

          return {
            class: classString,
            relations,
          };
        }
      )
    )
  ).reduce(
    (acc, elem) => ({
      classes: [...acc.classes, elem.class],
      relations: [...acc.relations, ...elem.relations],
    }),
    {
      classes: [] as string[],
      relations: [] as string[],
    }
  );

  return umlBoilerplate(multiline`
    ${classes.join("\n")}
    ${relations.join("\n")}
  `);
}

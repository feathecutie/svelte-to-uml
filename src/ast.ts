import {
  Root,
  Paragraph,
  Text,
  Link,
  Heading,
  Code,
  InlineCode,
  Image,
  Html,
} from "mdast";

type Args<T> = Omit<T, "type" | "position" | "data">;

export const rootNode = (args: Args<Root>): Root => ({
  type: "root",
  ...args,
});

export const paragraphNode = (args: Args<Paragraph>): Paragraph => ({
  type: "paragraph",
  ...args,
});

export const textNode = (args: Args<Text>): Text => ({
  type: "text",
  ...args,
});

export const linkNode = (args: Args<Link>): Link => ({
  type: "link",
  ...args,
});

export const headingNode = (args: Args<Heading>): Heading => ({
  type: "heading",
  ...args,
});

export const codeNode = (args: Args<Code>): Code => ({
  type: "code",
  ...args,
});

export const inlineCodeNode = (args: Args<InlineCode>): InlineCode => ({
  type: "inlineCode",
  ...args,
});

export const imageNode = (args: Args<Image>): Image => ({
  type: "image",
  ...args,
});

export const htmlNode = (args: Args<Html>): Html => ({
  type: "html",
  ...args,
});

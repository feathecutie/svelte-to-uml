import { Brand, Context, Option } from "effect";
import path from "node:path";
import ts from "typescript";
import {
  ImportIds,
  Type,
  Name,
  Props,
  Generics,
  Documentation,
  Slots,
  Events,
} from "./processSvelteFile.js";

export type AbsolutePath = Brand.Branded<string, "AbsolutePath">;
export const AbsolutePath = Brand.refined<AbsolutePath>(
  (p) => path.isAbsolute(p),
  (p) => Brand.error(`Expected ${p} to be an absolute path`)
);

export type Id = Brand.Branded<string, "Id">;

export type SvelteFileData = {
  id: Id;
  packageId: Option.Option<ts.PackageId>;
  importIds: ImportIds;
  type: Type;
  name: Name;
  props: Props;
  generics: Generics;
  documentation: Documentation;
  slots: Slots;
  events: Events;
};

export class TypeChecker extends Context.Tag("TypeChecker")<
  TypeChecker,
  ts.TypeChecker
>() {}
export class Program extends Context.Tag("Program")<Program, ts.Program>() {}
export class Options extends Context.Tag("Options")<
  Options,
  ts.CompilerOptions
>() {}
export class ProjectPath extends Context.Tag("ProjectPath")<
  ProjectPath,
  AbsolutePath
>() {}
export class OutPath extends Context.Tag("OutPath")<OutPath, AbsolutePath>() {}

import ts from "typescript";
import path from "node:path";
import fs from "node:fs";
import module from "node:module";
import { isSvelteFilepath, isVirtualSvelteFilepath } from "./utils.js";
import { AbsolutePath } from "./types.js";
import svelteSys, { getIsTsFile } from "./system.js";

export function resolveModuleName(
  name: string,
  containingFile: string,
  compilerOptions: ts.CompilerOptions
) {
  const tsResolvedModule = ts.resolveModuleName(
    name,
    containingFile,
    compilerOptions,
    ts.sys
  ).resolvedModule;
  if (
    tsResolvedModule &&
    !isVirtualSvelteFilepath(tsResolvedModule.resolvedFileName)
  ) {
    return tsResolvedModule;
  }
  return ts.resolveModuleName(name, containingFile, compilerOptions, svelteSys)
    .resolvedModule;
}

export function initializeTypescript(projectPath: AbsolutePath) {
  const require = module.createRequire(import.meta.url);

  let configPath = path.join(projectPath, "tsconfig.json");
  if (!fs.existsSync(configPath))
    configPath = path.join(projectPath, "jsconfig.json");

  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/unbound-method
  const { config: tsConfig } = ts.readConfigFile(configPath, ts.sys.readFile);

  const { options, fileNames } = ts.parseJsonConfigFileContent(
    tsConfig,
    // TODO: Try evaluating with svelteSys instead? Maybe then the resolving to .svelte files for default values works
    ts.sys,
    // svelteSys,
    projectPath,
    { sourceMap: false },
    configPath,
    undefined,
    [
      {
        extension: "svelte",
        isMixedContent: true,
        scriptKind: ts.ScriptKind.Deferred,
      },
    ]
  );

  const filenames = fileNames.map((name) => {
    if (!isSvelteFilepath(name)) return name;
    return name + (getIsTsFile(name) ? ".ts" : ".js");
  });
  filenames.push(require.resolve("svelte2tsx/svelte-shims-v4.d.ts"));

  const host = ts.createIncrementalCompilerHost(options, svelteSys);
  host.resolveModuleNameLiterals = (
    moduleLiterals,
    containingFile,
    _redirectedReference,
    options
  ) => {
    return moduleLiterals.map((moduleLiteral) => ({
      resolvedModule: resolveModuleName(
        moduleLiteral.text,
        containingFile,
        options
      ),
    }));
  };
  const program = ts.createProgram(filenames, options, host);

  const tc = program.getTypeChecker();

  return {
    options,
    program,
    tc,
  };
}

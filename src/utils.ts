import multiline from "multiline-ts";
import fs from "node:fs";
import path from "node:path";

export function isSvelteFilepath(filePath: string) {
  return filePath.endsWith(".svelte");
}

export function isVirtualSvelteFilepath(filePath: string) {
  return filePath.endsWith(".svelte.ts") || filePath.endsWith("svelte.js");
}

function toRealSvelteFilepath(filePath: string) {
  return filePath.slice(0, -3); // -'.js'.length || -'.ts'.length
}

export function ensureRealSvelteFilepath(filePath: string) {
  return isVirtualSvelteFilepath(filePath)
    ? toRealSvelteFilepath(filePath)
    : filePath;
}

/**
 * Intended to be used as a template string literal tag, evaluates to empty string if not all expressions are truthy.
 * @example
 * assert.strictEqual(opt`A string with a ${"truthy"} value and a ${null} value`, "");
 * @example
 * assert.strictEqual(opt`A string with a ${"truthy"} value and ${"another one"}`, "A string with a truthy value and another one");
 * @param strings
 * @param exps
 * @returns The same result as `multiline-ts` if all interpolated expressions are truthy, or an empty string otherwise
 */
export function opt(
  strings: TemplateStringsArray,
  ...exps: ({ text: string; considered: boolean } | string | null | undefined)[]
) {
  if (
    exps.every(
      (v) =>
        v && (typeof v === "string" ? v.length : !v.considered || v.text.length)
    )
  ) {
    return multiline(
      strings,
      ...exps.filter(Boolean).map((v) => (typeof v === "string" ? v : v.text))
    );
  }
  return "";
  // return exps.every(Boolean) ? multiline(strings, ...exps) : "";
}

/**
 * Memoizes the given function, storing calculated results in memory.
 * @param func The function to memoize.
 * @returns The memoized function.
 */
export function memoize<T>(func: (arg: string) => T): (arg: string) => T {
  const cache: Record<string, T> = {};
  return (x) => {
    if (x in cache) return cache[x];
    return (cache[x] = func(x));
  };
}

export function writeToFileSync(filePath: string, contents: string) {
  fs.mkdirSync(path.dirname(filePath), { recursive: true });
  fs.writeFileSync(filePath, contents);
}

#!/usr/bin/env node
import { Effect, Context } from "effect";
import { Command, Args } from "@effect/cli";
import { NodeContext, NodeRuntime } from "@effect/platform-node";
import { initializeTypescript } from "./init-ts.js";
import {
  AbsolutePath,
  Options,
  OutPath,
  Program,
  ProjectPath,
  TypeChecker,
} from "./types.js";
import main from "./main.js";
import path from "node:path";

const package_name =
  process.env.npm_package_name?.split("/").at(-1) ?? "svelte-to-uml";
const package_version = process.env.npm_package_version ?? "0.1.0";

const command = Command.make(
  package_name,
  {
    projectPath: Args.directory({
      name: "PROJECT_PATH",
      exists: "yes",
    }).pipe(
      Args.withDescription("Path to SvelteKit project."),
      Args.map((p) => path.resolve(p)),
      Args.map(AbsolutePath)
    ),
    outPath: Args.directory({ name: "OUT_DIR", exists: "either" }).pipe(
      Args.withDescription("Directory to generate docs in."),
      Args.map((p) => path.resolve(p)),
      Args.map(AbsolutePath)
    ),
  },
  ({ projectPath, outPath }) =>
    Effect.gen(function* (_) {
      const { tc, program, options } = initializeTypescript(projectPath);
      const context = Context.empty().pipe(
        Context.add(ProjectPath, projectPath),
        Context.add(OutPath, outPath),
        Context.add(TypeChecker, tc),
        Context.add(Program, program),
        Context.add(Options, options)
      );

      yield* _(
        Effect.provide(main, context),
        Effect.tap(Effect.log("Program finished"))
      );
    }).pipe(Effect.withLogSpan("program"))
);

const cli = Command.run(command, {
  name: package_name,
  version: package_version,
});

Effect.suspend(() => cli(process.argv)).pipe(
  Effect.provide(NodeContext.layer),
  NodeRuntime.runMain
);

/* eslint-disable */
import js from "@eslint/js";
import jsdoc from "eslint-plugin-jsdoc";
import { FlatCompat } from "@eslint/eslintrc";
import path from "node:path";
import { fileURLToPath } from "node:url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const compat = new FlatCompat({
  baseDirectory: __dirname,
  recommendedConfig: js.configs.recommended,
});

export default [
  js.configs.recommended,
  ...compat.config({
    extends: [
      // "plugin:@typescript-eslint/recommended",
      "plugin:@typescript-eslint/recommended-type-checked",
      "plugin:@typescript-eslint/stylistic-type-checked",
    ],
    parser: "@typescript-eslint/parser",
    parserOptions: {
      project: true,
      tsconfigRootDir: __dirname,
    },
    plugins: ["@typescript-eslint"],
    root: true,
    rules: {
      "@typescript-eslint/consistent-type-definitions": ["error", "type"],
    },
  }),
  // jsdoc.configs["flat/recommended-typescript"],
  // {
  //   rules: {
  //     // "jsdoc/require-param-description": "off",
  //     // "jsdoc/require-returns-description": "off",
  //   },
  // },
];
